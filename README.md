## Zabbix API package

This package is using to comfort work with zabbix API system in the Telecloud ecosystem.

## Installation:

### Via Composer:

Package installing:
```bash
composer require konstantinkotov/zabbix-api-package
```

### Package settings:

```bash
php artisan zabbix:install
```

### .env params:
```env
ZABBIX_BASE_POINT=<zabbix base endpoint>
ZABBIX_API_KEY=<zabbix api key>
```

## How to use:

All requests accept a filled instances of classes implementing HasBuildInterface. Excluding 

**Main objects:**\
class HostDirector\
class HostGroupParamsBuilder\
class TriggerParamsBuilder\
class TemplateParamsBuilder

**Support objects:**\
class HostGroupBuilder\
class HostTemplateBuilder\
class HostInterfacesBuilder

**Additional objects:**\
Enum HostField\
Enum HostGroupField\
Enum TriggerField\
Enum TriggerPriority\
Enum TriggerValue\
Enum TriggerStatus\
Enum TriggerState

### Hosts:

#### Get:

First, you need to create a host director and set necessary filters.
All fields for the filters you can find in _Enum HostField_.
Then, you must make a request. For example:

```php
use Konstantinkotov\ZabbixApiPackage\ZabbixService;
use Konstantinkotov\ZabbixApiPackage\Builders\Hosts\HostDirector;
use Konstantinkotov\ZabbixApiPackage\Enums\Hosts\HostField;

$hostIds = [<your_first_host_id>, <your_second_host_id>, ...];

$hostDirector = new HostDirector();
$hostDirector->addFilter([
    HostField::HOST_ID->value => $hostIds,
    HostField::NAME->value => <your_necessary_host_name>,
    "<necessary_field_name>" => "<necessary_field_value>",
    ...
]);

$hostDirector->addFilter([HostField::HOST_ID->value => $hostIds]);
$hostDirector->addFilter([HostField::NAME->value => <searchable_host_name>]);

array|stdClass $response = ZabbixService::hosts()->get($hostDirector, ?bool $associative);
```

#### Create:
First, you need to create all necessary builders and make a request, include all builders to host director and make a request.
For example:

```php
use Konstantinkotov\ZabbixApiPackage\Builders\Hosts\HostInterfacesBuilder;
use Konstantinkotov\ZabbixApiPackage\Builders\Hosts\HostGroupBuilder;
use Konstantinkotov\ZabbixApiPackage\Builders\Hosts\HostTemplateBuilder;
use Konstantinkotov\ZabbixApiPackage\Builders\Hosts\HostDirector;
use Konstantinkotov\ZabbixApiPackage\ZabbixService;

$hostInterfaceBuilder = new HostInterfacesBuilder();
$hostInterfaceBuilder->setType();
$hostInterfaceBuilder->setMain();
$hostInterfaceBuilder->setUseIp();
$hostInterfaceBuilder->setIp(<your_server_ip>);
$hostInterfaceBuilder->setDns();
$hostInterfaceBuilder->setPort();
$hostInterfaceBuilder->setInterfaceRef(<?your_host_interface>);

$hostGroupBuilder = new HostGroupBuilder();
$hostGroupBuilder->setGroupId(<your_group_id>);

$hostTemplateBuilder = new HostTemplateBuilder();
$hostTemplateBuilder->setTemplateId(<your_template_id>);

$hostDirector = new HostDirector();
$hostDirector->setHostName(<your_host_name>);
$hostDirector->setInterfaces($hostInterfaceBuilder);
$hostDirector->setGroups($hostGroupBuilder);
$hostDirector->setTemplates($hostTemplateBuilder);

array|stdClass $response = ZabbixService::hosts()->create($hostDirector);
```


#### Update:
First, you need to create a host director with necessary data array. You must set a required mutable host id. Available fields you can find in _Enum HostFiend_
Then, you need to make a request. 
For example:

```php
use Konstantinkotov\ZabbixApiPackage\Builders\Hosts\HostDirector;
use Konstantinkotov\ZabbixApiPackage\ZabbixService;
use Konstantinkotov\ZabbixApiPackage\Enums\Hosts\HostField;
use Konstantinkotov\ZabbixApiPackage\Enums\Hosts\HostStatus;

$hostDirector = new HostDirector();
$hostDirector->addParams([
    HostField::HOST_ID->value => <your_mutable_host_id>,
    HostField::NAME->value => <your_new_host_name>,
    HostField::STATUS->value => HostStatus::ENABLED,
    "<necessary_field_name>" => "<necessary_field_value>",
    ...
]);

array|stdClass $response = ZabbixService::hosts()->update($hostDirector);
```


#### Delete:
First, you need to create a host director set filter with required host id and make a request.
For example:

```php
use Konstantinkotov\ZabbixApiPackage\Builders\Hosts\HostDirector;
use Konstantinkotov\ZabbixApiPackage\Enums\Hosts\HostField;
use Konstantinkotov\ZabbixApiPackage\ZabbixService;

$hostDirector = new HostDirector();
$hostDirector->addFilter([HostField::HOST_ID->value => <your_host_id>]);

array|stdClass $response = ZabbixService::hosts()->delete($hostDirector); 
```

### Host Group:

#### Get:
You could create a host group params builder and make a request. Your can also make the request without the group params builder.
For example (with GroupParamsBuilder instance):

```php
use Konstantinkotov\ZabbixApiPackage\Builders\Hosts\Groups\HostGroupParamsBuilder;
use Konstantinkotov\ZabbixApiPackage\Enums\Hosts\Groups\HostGroupField;
use Konstantinkotov\ZabbixApiPackage\ZabbixService;

$hostGroupParamsBuilder = new HostGroupParamsBuilder();
$hostGroupParamsBuilder->addOutput(HostGroupField::NAME);
$hostGroupParamsBuilder->addOutput(HostGroupField::FLAGS);
...

array|stdClass $response = ZabbixService::hosts()->groups()->get($hostGroupParamsBuilder, ?bool $associative);
```

### Triggers

#### Get:
You must create a trigger params builder, set necessary params and make a request.
For example:

```php
use Konstantinkotov\ZabbixApiPackage\Builders\Triggers\TriggerParamsBuilder;
use Konstantinkotov\ZabbixApiPackage\Enums\Triggers\TriggerField;
use Konstantinkotov\ZabbixApiPackage\Enums\Triggers\TriggerPriority;
use Konstantinkotov\ZabbixApiPackage\Enums\Triggers\TriggerValue;
use Konstantinkotov\ZabbixApiPackage\Enums\Triggers\TriggerStatus;
use Konstantinkotov\ZabbixApiPackage\Enums\Triggers\TriggerState;
use Konstantinkotov\ZabbixApiPackage\ZabbixService;

$triggerParamsBuilder = new TriggerParamsBuilder();
$triggerParamsBuilder->addHostId(<your_first_host_id>);
$triggerParamsBuilder->addHostId(<your_second_host_id>);
...

$priorities = [TriggerPriority::HIGH, TriggerPriority::DISASTER];

$filterArray = [
    TriggerField::PRIORITY->value => $priorities,
    TriggerField::VALUE->value => TriggerValue::HAS_PROBLEM,
    TriggerField::STATUS->value => TriggerStatus::ENABLED,
    TriggerField::STATE->value => TriggerState::UNKNOWN,
    "<necessary_field_name>" => "<necessary_field_value>",
    ...
];

$triggerParamsBuilder->addFilter($filterArray);
$triggerParamsBuilder->addFilter([TriggerField::VALUE->value => TriggerValue::OK]);
$triggerParamsBuilder->addFilter([TriggerField::STATUS->value => TriggerStatus::ENABLED]);
...

$triggerParamsBuilder->setSortField(TriggerField::PRIORITY);
$triggerParamsBuilder->setSortOrder('DESC'|'ASC');

array|stdClass $response = ZabbixService::triggers()->get($triggerParamsBuilder, ?bool $associative);
```

### Templates

#### Get:
You must create a template params builder, set necessary params and make a request.
For example:

```php
use Konstantinkotov\ZabbixApiPackage\Builders\Templates\TemplateParamsBuilder;
use \Konstantinkotov\ZabbixApiPackage\Enums\Templates\TemplateField;
use Konstantinkotov\ZabbixApiPackage\ZabbixService;

$templateParamsBuilder = new TemplateParamsBuilder();
$templateParamsBuilder->addOutput(TemplateField::HOST);
$templateParamsBuilder->addOutput(TemplateField::TEMPLATE_ID);
...

array|stdClass $response = ZabbixService::templates()->get($templateParamsBuilder, ?bool $associative);
```

### Items

#### Get:
You must create an item params builder, set necessary params and make a request.
For example:

```php
use Konstantinkotov\ZabbixApiPackage\Builders\Items\ItemParamsBuilder;
use Konstantinkotov\ZabbixApiPackage\Enums\Items\ItemField;
use Konstantinkotov\ZabbixApiPackage\ZabbixService;

$itemParamsBuilder = new ItemParamsBuilder();
$itemParamsBuilder->addOutput(ItemField::HOST);
$itemParamsBuilder->addOutput(ItemField::NAME);
...
$itemParamsBuilder->addFilter([ItemField::HOST_ID => <your_host_id>]);
...
$itemParamsBuilder->addFilter([ItemField::NAME => [<interface_fullname>, ...]]);
...

array|stdClass $response = ZabbixService::items()->get($itemParamsBuilder, ?bool $associative);
```

### Dashboards

#### Get:
You must create a dashboard params builder, set necessary params and make a request.
For example:

```php
use Konstantinkotov\ZabbixApiPackage\Builders\Dashboards\DashboardParamsBuilder
use Konstantinkotov\ZabbixApiPackage\Enums\Dashboards\DashboardField;
use Konstantinkotov\ZabbixApiPackage\ZabbixService;

$dashboardParamsBuilder = new DashboardParamsBuilder();
$dashboardParamsBuilder->addOutput(DashboardField::NAME);
$dashboardParamsBuilder->addOutput(DashboardField::DASHBOARD_ID);
...
$dashboardParamsBuilder->addFilter([DashboardField::DASHBOARD_ID]);
$dashboardParamsBuilder->addFilter([DashboardField::SELECT_PAGES => [DashboardField::WIDGETS]]);
...

array|stdClass $response = ZabbixService::dashboard()->get($dashboardParamsBuilder, ?bool $associative);
```

#### Update:
First, you need to create a dashboard params builder, set a dashboard id, create a page params builder, 
set dashboard id, add all necessary widgets and make a request.
For example:

```php
use Konstantinkotov\ZabbixApiPackage\Builders\Dashboards\DashboardParamsBuilder;
use Konstantinkotov\ZabbixApiPackage\Builders\Dashboards\Pages\PageParamsBuilder;
use Konstantinkotov\ZabbixApiPackage\Builders\Dashboards\Pages\Widgets\WidgetParamsBuilder;
use Konstantinkotov\ZabbixApiPackage\Builders\Dashboards\Pages\Widgets\WidgetFieldItemBuilder;

use Konstantinkotov\ZabbixApiPackage\Enums\Dashboards\DashboardField;
use Konstantinkotov\ZabbixApiPackage\Enums\Dashboards\Widgets\WidgetType;
use Konstantinkotov\ZabbixApiPackage\Enums\Dashboards\Widgets\WidgetFieldItemType;
use Konstantinkotov\ZabbixApiPackage\ZabbixService;

$dashboardParamsBuilder = new DashboardParamsBuilder();
$dashboardParamsBuilder->setDashboardId(<your_dashboard_id>);

$pageParamsBuilder = new PageParamsBuilder();
$pageParamsBuilder->setDashboardPageId(<your_dashboard_pageid>);
$pageParamsBuilder->setWidgets(array<WidgetParamsBuilder::class>);

// or you can create a new widget
$widgetParamsBuilder = new WidgetParamsBuilder();
$widgetParamsBuilder->setName(<your_widget_name>);
$widgetParamsBuilder->setType(WidgetType::SVG_GRAPH);
$widgetParamsBuilder->setPositionX(<?your_X_coordinates>);
$widgetParamsBuilder->setPositionY(<?your_Y_coordinates>);
$widgetParamsBuilder->setWidth(<your_widget_width>);
$widgetParamsBuilder->setHeight(<your_widget_height>);

$widgetFieldItemBuilder = new WidgetFieldItemBuilder();
$widgetFieldItemBuilder->setType(WidgetFieldItemType::STRING);
$widgetFieldItemBuilder->setName(<your_widget_field_name>);
$widgetFieldItemBuilder->setValue(<your_widget_field_value>);

$widgetParamsBuilder->addField($widgetFieldItemBuilder);

$pageParamsBuilder->addWidget($widgetParamsBuilder);
...
$dashboardParamsBuilder->setPages($pageParamsBuilder);

array|stdClass $response = ZabbixService::dashboard()->update($dashboardParamsBuilder, ?bool $associative);
```



