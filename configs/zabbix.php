<?php

return [
    'base_point' => env('ZABBIX_BASE_POINT'),
    'api_key' => env('ZABBIX_API_KEY'),
    'jsonrpc_version' => env('ZABBIX_JSONRPC_VERSION', '2.0'),
    "request_id" => env('ZABBIX_REQUEST_ID', 1),
    'endpoints' => [
        'hosts' => [
            'create' => 'host.create',
            'get' => 'host.get',
            'update' => 'host.update',
            'delete' => 'host.delete',
            'groups' => [
                'get' => 'hostgroup.get'
            ]
        ],
        'triggers' => [
            'get' => 'trigger.get'
        ],
        'templates' => [
            'get' => 'template.get'
        ],
        'items' => [
            'get' => 'item.get'
        ],
        'dashboards' => [
            'get' => 'dashboard.get',
            'create' => 'dashboard.create',
            'update' => 'dashboard.update',
        ]
    ]
];
