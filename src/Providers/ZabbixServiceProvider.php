<?php

namespace Konstantinkotov\ZabbixApiPackage\Providers;

use Carbon\Laravel\ServiceProvider;
use Konstantinkotov\ZabbixApiPackage\Commands\ZabbixInstall;

class ZabbixServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../../configs/zabbix.php' => config_path('zabbix.php')
        ]);

        $this->commands([
            ZabbixInstall::class
        ]);
    }
}