<?php

namespace Konstantinkotov\ZabbixApiPackage\Actions;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Request;
use stdClass;

class SendAction
{
    /**
     * The action with a code for make a request to the zabbix server.
     *
     * @param array $data
     * @param bool $associative
     * @return array|stdClass
     * @throws GuzzleException
     */
    public static function send(array $data, bool $associative): array|stdClass
    {
        $headers = [
            'Content-Type' => 'application/json-rpc',
            'Authorization' => 'Bearer ' . config('zabbix.api_key')
        ];
        $request = new Request('GET', config('zabbix.base_point'), $headers, json_encode($data));

        return json_decode((new Client())->send($request)->getBody(), $associative);
    }
}
