<?php

namespace Konstantinkotov\ZabbixApiPackage\Commands;

use Illuminate\Console\Command;
use Konstantinkotov\ZabbixApiPackage\Providers\ZabbixServiceProvider;
use Symfony\Component\Console\Command\Command as CommandAlias;

class ZabbixInstall extends Command
{
    protected $signature = 'zabbix:install';
    protected $description = 'The zabbix installation command';

    public function handle()
    {
        $this->call('vendor:publish', [
            '--provider' => ZabbixServiceProvider::class,
            '--force'    => true
        ]);

        return CommandAlias::SUCCESS;
    }
}