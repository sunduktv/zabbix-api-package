<?php

namespace Konstantinkotov\ZabbixApiPackage\Applications;

use GuzzleHttp\Exception\GuzzleException;
use Konstantinkotov\ZabbixApiPackage\Actions\SendAction;
use Konstantinkotov\ZabbixApiPackage\Applications\Contracts\CoreApplicationInterface;
use stdClass;

class CoreApplication implements CoreApplicationInterface
{
    protected array $data;

    /**
     * The action to make a final request.
     *
     * @param bool $associative
     * @return array|stdClass
     * @throws GuzzleException
     */
    public function send(bool $associative = true) : array|stdClass
    {
        return SendAction::send($this->data, $associative);
    }
}