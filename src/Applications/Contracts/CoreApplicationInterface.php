<?php

namespace Konstantinkotov\ZabbixApiPackage\Applications\Contracts;

use stdClass;

interface CoreApplicationInterface
{
    /**
     * The action to make final request.
     *
     * @param bool $associative
     * @return array|stdClass
     */
    public function send(bool $associative) : array|stdClass;
}