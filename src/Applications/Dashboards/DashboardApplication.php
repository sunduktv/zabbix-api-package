<?php

namespace Konstantinkotov\ZabbixApiPackage\Applications\Dashboards;

use Konstantinkotov\ZabbixApiPackage\Applications\CoreApplication;
use Konstantinkotov\ZabbixApiPackage\Builders\Contracts\HasBuildInterface;
use stdClass;

class DashboardApplication extends CoreApplication
{
    /**
     * @param HasBuildInterface $dashboardParamsBuilder
     * @param bool $associative
     * @return array|stdClass
     * @throws GuzzleException
     */
    public function create(HasBuildInterface $dashboardParamsBuilder, bool $associative = true) : array|stdClass
    {
        $this->data = [
            'jsonrpc' => config('zabbix.jsonrpc_version'),
            'method' => config('zabbix.endpoints.dashboards.create'),
            'params' => $dashboardParamsBuilder->build(),
            'id' => config('zabbix.request_id')
        ];

        return $this->send($associative);
    }

    /**
     * @param HasBuildInterface $dashboardParamsBuilder
     * @param bool $associative
     * @return array|stdClass
     * @throws GuzzleException
     */
    public function update(HasBuildInterface $dashboardParamsBuilder, bool $associative = true) : array|stdClass
    {
        $this->data = [
            'jsonrpc' => config('zabbix.jsonrpc_version'),
            'method' => config('zabbix.endpoints.dashboards.update'),
            'params' => $dashboardParamsBuilder->build(),
            'id' => config('zabbix.request_id')
        ];

        return $this->send($associative);
    }

    /**
     * @param HasBuildInterface $dashboardParamsBuilder
     * @param bool $associative
     * @return array|stdClass
     * @throws GuzzleException
     */

    public function get(HasBuildInterface $dashboardParamsBuilder, bool $associative = true) : array|stdClass
    {
        $this->data = [
            'jsonrpc' => config('zabbix.jsonrpc_version'),
            'method' => config('zabbix.endpoints.dashboards.get'),
            'params' => $dashboardParamsBuilder->build(),
            'id' => config('zabbix.request_id')
        ];

        return $this->send($associative);
    }
}