<?php

namespace Konstantinkotov\ZabbixApiPackage\Applications\Hosts;

use GuzzleHttp\Exception\GuzzleException;
use Konstantinkotov\ZabbixApiPackage\Applications\CoreApplication;
use Konstantinkotov\ZabbixApiPackage\Builders\Contracts\DataBuilderCoreInterface;
use Konstantinkotov\ZabbixApiPackage\Builders\Contracts\HasBuildInterface;
use stdClass;

class GroupApplication extends CoreApplication
{
    /**
     * @param DataBuilderCoreInterface|null $groupParamsBuilder
     * @return array|stdClass
     * @throws GuzzleException
     */
    public function get(HasBuildInterface $groupParamsBuilder = null, bool $associative = true) : array|stdClass
    {
            $this->data = [
            'jsonrpc' => config('zabbix.jsonrpc_version'),
            'method' => config('zabbix.endpoints.hosts.groups.get'),
            'params' => $groupParamsBuilder->build(),
            'id' => config('zabbix.request_id')
        ];

        return $this->send($associative);
    }
}