<?php

namespace Konstantinkotov\ZabbixApiPackage\Applications\Hosts;

use GuzzleHttp\Exception\GuzzleException;
use Konstantinkotov\ZabbixApiPackage\Applications\CoreApplication;
use Konstantinkotov\ZabbixApiPackage\Builders\Contracts\HasBuildInterface;
use stdClass;

class HostApplication extends CoreApplication
{
    /**
     * The method to create new host on the zabbix server
     *
     * @param HasBuildInterface $hostDirector
     * @param bool $associative
     * @return array|stdClass
     * @throws GuzzleException
     */
    public function create(HasBuildInterface $hostDirector, bool $associative = true) : array|stdClass
    {
        $this->data = [
            'jsonrpc' => config('zabbix.jsonrpc_version'),
            'method' => config('zabbix.endpoints.hosts.create'),
            'params' => $hostDirector->build(),
            'id' => config('zabbix.request_id')
        ];

        return $this->send($associative);
    }


    /**
     * The method to update a host on the zabbix server.
     *
     * @param HasBuildInterface $hostDirector
     * @param bool $associative
     * @return array|stdClass
     * @throws GuzzleException
     */
    public function update(HasBuildInterface $hostDirector, bool $associative = true) : array|stdClass
    {
        $this->data = [
            'jsonrpc' => config('zabbix.jsonrpc_version'),
            'method' => config('zabbix.endpoints.hosts.update'),
            'params' => $hostDirector->build(),
            'id' => config('zabbix.request_id')
        ];

        return $this->send($associative);
    }

    /**
     * The method to get information about the hosts
     *
     * In $hosts - could be both array and string.
     *
     * @param HasBuildInterface $hostDirector
     * @param bool $associative
     * @return array|stdClass
     * @throws GuzzleException
     */

    public function get(HasBuildInterface $hostDirector, bool $associative = true) : array|stdClass
    {
        $this->data = [
            'jsonrpc' => config('zabbix.jsonrpc_version'),
            'method' => config('zabbix.endpoints.hosts.get'),
            'params' => $hostDirector->build(),
            'id' => config('zabbix.request_id')
        ];

        return $this->send($associative);
    }

    /**
     * The method to delete host from the server.
     *
     * @param HasBuildInterface $hostDirector
     * @param bool $associative
     * @return array|stdClass
     * @throws GuzzleException
     */

    public function delete(HasBuildInterface $hostDirector, bool $associative = true) : array|stdClass
    {
        $this->data = [
            'jsonrpc' => config('zabbix.jsonrpc_version'),
            'method' => config('zabbix.endpoints.hosts.delete'),
            'params' => $hostDirector->build(),
            'id' => config('zabbix.request_id')
        ];

        return $this->send($associative);
    }

    public function groups() : GroupApplication
    {
        return (new GroupApplication());
    }
}
