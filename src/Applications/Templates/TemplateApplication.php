<?php

namespace Konstantinkotov\ZabbixApiPackage\Applications\Templates;

use Konstantinkotov\ZabbixApiPackage\Applications\CoreApplication;
use Konstantinkotov\ZabbixApiPackage\Builders\Contracts\HasBuildInterface;
use stdClass;

class TemplateApplication extends CoreApplication
{
    /**
     *
     * @param HasBuildInterface $paramsBuilder
     * @param bool $associative
     * @return array|stdClass
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function get(HasBuildInterface $paramsBuilder, bool $associative = true): array|stdClass {
        $this->data = [
            'jsonrpc' => config('zabbix.jsonrpc_version'),
            'method' => config('zabbix.endpoints.templates.get'),
            'params' => $paramsBuilder->build(),
            "id" => config('zabbix.request_id')
        ];

        return $this->send($associative);
    }
}