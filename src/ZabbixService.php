<?php

namespace Konstantinkotov\ZabbixApiPackage;

use Konstantinkotov\ZabbixApiPackage\Applications\Dashboards\DashboardApplication;
use Konstantinkotov\ZabbixApiPackage\Applications\Hosts\HostApplication;
use Konstantinkotov\ZabbixApiPackage\Applications\Items\ItemApplication;
use Konstantinkotov\ZabbixApiPackage\Applications\Templates\TemplateApplication;
use Konstantinkotov\ZabbixApiPackage\Applications\Triggers\TriggerApplication;

class ZabbixService
{
    public static function hosts(): HostApplication
    {
        return (new HostApplication());
    }

    public static function triggers(): TriggerApplication
    {
        return (new TriggerApplication());
    }

    public static function templates(): TemplateApplication
    {
        return (new TemplateApplication());
    }

    public static function items(): ItemApplication
    {
        return (new ItemApplication());
    }

    public static function dashboard(): DashboardApplication
    {
        return (new DashboardApplication());
    }
}
