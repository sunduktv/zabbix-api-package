<?php

namespace Konstantinkotov\ZabbixApiPackage\Enums\Templates;

enum TemplateField: string
{
    case EXTEND = "extend";
    case PROXY_HOST_ID = "proxy_hostid";
    case HOST = "host";
    case STATUS = "status";
    case IPMI_AUTH_TYPE = "ipmi_authtype";
    case IPMI_PRIVILEGE = "ipmi_privilege";
    case IPMI_USERNAME = "ipmi_username";
    case IPMI_PASSWORD = "ipmi_password";
    case MAINTENANCE_ID = "maintenanceid";
    case MAINTENANCE_STATUS = "maintenance_status";
    case MAINTENANCE_TYPE = "maintenance_type";
    case MAINTENANCE_FROM = "maintenance_from";
    case NAME = "name";
    case FLAGS = "flags";
    case TEMPLATE_ID = "templateid";
    case DESCRIPTION = "description";
    case TLS_CONNECT = "tls_connect";
    case TLS_ACCEPT = "tls_accept";
    case TLS_ISSUER = "tls_issuer";
    case TLS_SUBJECT = "tls_subject";
    case TLS_PSK_IDENTITY = "tls_psk_identity";
    case TLS_PSK = "tls_psk";
    case PROXY_ADDRESS = "proxy_address";
    case AUTO_COMPRESS = "auto_compress";
    case CUSTOM_INTERFACES = "custom_interfaces";
    case UUID = "uuid";
    case VENDOR_NAME = "vendor_name";
    case VENDOR_VERSION = "vendor_version";
}
