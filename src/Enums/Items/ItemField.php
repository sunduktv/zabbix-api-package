<?php

namespace Konstantinkotov\ZabbixApiPackage\Enums\Items;

enum ItemField: string
{
    case ITEM_ID = "itemid";
    case TYPE = "type";
    case SNMP_OID = "snmp_oid";
    case HOST_ID = "hostid";
    case NAME = "name";
    case KEY = "key_";
    case DELAY = "delay";
    case HISTORY = "history";
    case TRENDS = "trends";
    case STATUS = "status";
    case VALUE_TYPE = "value_type";
    case TRAPPER_HOSTS = "trapper_hosts";
    case UNITS = "units";
    case FORMULA = "formula";
    case LOG_TIME_FMT = "logtimefmt";
    case TEMPLATE_ID = "templateid";
    case VALUE_MAP_ID = "valuemapid";
    case PARAMS = "params";
    case IPMI_SENSOR = "ipmi_sensor";
    case AUTH_TYPE = "authtype";
    case USERNAME = "username";
    case PASSWORD = "password";
    case PUBLIC_KEY = "publickey";
    case PRIVATE_KEY = "privatekey";
    case FLAGS = "flags";
    case INTERFACE_ID = "interfaceid";
    case DESCRIPTION = "description";
    case INVENTORY_LINK = "inventory_link";
    case LIFE_TIME = "lifetime";
    case EVAL_TYPE = "evaltype";
    case JMX_ENDPOINT = "jmx_endpoint";
    case MASTER_ITEM_ID = "master_itemid";
    case TIMEOUT = "timeout";
    case URL = "url";
    case QUERY_FIELDS = "query_fields";
    case POSTS = "posts";
    case STATUS_CODES = "status_codes";
    case FOLLOW_REDIRECTS = "follow_redirects";
    case POST_TYPE = "post_type";
    case HTTP_PROXY = "http_proxy";
    case HEADERS = "headers";
    case RETRIEVE_MODE = "retrieve_mode";
    case REQUEST_METHOD = "request_method";
    case OUTPUT_FORMAT = "output_format";
    case SSL_CERT_FILE = "ssl_cert_file";
    case SSL_KEY_FILE = "ssl_key_file";
    case SSL_KEY_PASSWORD = "ssl_key_password";
    case VERIFY_PEER = "verify_peer";
    case VERIFY_HOST = "verify_host";
    case ALLOW_TRAPS = "allow_traps";
    case UUID = "uuid";
    case STATE = "state";
    case ERROR = "error";
    case PARAMETERS = "parameters";
    case LAST_CLOCK = "lastclock";
    case LAST_NS = "lastns";
    case LAST_VALUE = "lastvalue";
    case PREV_VALUE = "prevvalue";
}
