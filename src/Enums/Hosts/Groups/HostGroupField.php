<?php

namespace Konstantinkotov\ZabbixApiPackage\Enums\Hosts\Groups;

enum HostGroupField: string
{
    case EXTEND = "extend";
    case GROUP_ID = 'groupid';
    case NAME = 'name';
    case FLAGS = 'flags';
    case UUID = 'uuid';
}
