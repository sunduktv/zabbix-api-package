<?php

namespace Konstantinkotov\ZabbixApiPackage\Enums\Hosts;

enum HostStatus : int
{
    case ENABLED = 0;
    case DISABLED = 1;
}
