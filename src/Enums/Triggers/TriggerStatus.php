<?php

namespace Konstantinkotov\ZabbixApiPackage\Enums\Triggers;

enum TriggerStatus : int
{
    case ENABLED = 0;
    case DISABLED = 1;
}
