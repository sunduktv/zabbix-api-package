<?php

namespace Konstantinkotov\ZabbixApiPackage\Enums\Triggers;

enum TriggerField: string
{
    case EXTEND = "extend";
    case TRIGGER_ID = 'triggerid';
    case EXPRESSION = 'expression';
    case DESCRIPTION = 'description';
    case URL = 'url';
    case STATUS = 'status';
    case VALUE = 'value';
    case PRIORITY = 'priority';
    case LAST_CHANGE = 'lastchange';
    case COMMENTS = 'comments';
    case ERROR = 'error';
    case TEMPLATE_ID = 'templateid';
    case TYPE = 'type';
    case STATE = 'state';
    case FLAGS = 'flags';
    case RECOVERY_MODE = 'recovery_mode';
    case RECOVERY_EXPRESSION = 'recovery_expression';
    case CORRELATION_MODE = 'correlation_mode';
    case CORRELATION_TAG = 'correlation_tag';
    case MANUAL_CLOSE = 'manual_close';
    case OP_DATA = 'opdata';
    case EVENT_NAME = 'event_name';
    case UUID = 'uuid';
    case URL_NAME = 'url_name';
}
