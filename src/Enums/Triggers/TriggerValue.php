<?php

namespace Konstantinkotov\ZabbixApiPackage\Enums\Triggers;

enum TriggerValue : int
{
    case HAS_PROBLEM = 1;
    case OK = 0;
}