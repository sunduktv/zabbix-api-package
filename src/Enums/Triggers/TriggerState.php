<?php

namespace Konstantinkotov\ZabbixApiPackage\Enums\Triggers;

enum TriggerState : int
{
    case NORMAL = 0;
    CASE UNKNOWN = 1;
}
