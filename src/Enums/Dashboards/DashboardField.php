<?php

namespace Konstantinkotov\ZabbixApiPackage\Enums\Dashboards;

enum DashboardField: string
{
    case EXTEND = 'extend';
    case DASHBOARD_ID = 'dashboardid';
    case NAME = 'name';
    case USER_ID = 'userid';
    case PRIVATE = 'private';
    case DISPLAY_PERIOD = 'display_period';
    case AUTO_START = 'auto_start';
    case UUID = 'uuid';
    case SELECT_PAGES = 'selectPages';
    case WIDGETS = 'widgets';
}
