<?php

namespace Konstantinkotov\ZabbixApiPackage\Enums\Dashboards\Widgets;

enum WidgetType : string
{
    case ACTION_LOG = 'actionlog';
    case CLOCK = 'clock';
    case DATA_OVER = 'dataover'; // deprecated
    case DISCOVERY = 'discovery';
    case FAV_GRAPHS = 'favgraphs';
    case FAV_MAPS = 'favmaps';
    case GAUGE = 'gauge';
    case GEOMAP = 'geomap';
    case GRAPH_CLASSIC = 'graph';
    case GRAPH_PROTOTYPE = 'graphprototype';
    case HONEYCOMB = 'honeycomb';
    case HOST_AVAILABILITY = 'hostavail';
    case HOST_NAVIGATOR = 'hostnavigator';
    case ITEM_HISTORY = 'itemhistory';
    case ITEM_NAVIGATOR = 'itemnavigator';
    case ITEM_VALUE = 'item';
    case MAP = 'map';
    case NAV_TREE = 'navtree';
    case PIE_CHART = 'piechart';
    case PROBLEM_HOSTS = 'problemhosts';
    case PROBLEMS = 'problems';
    case PROBLEMS_BY_SEVERITY = 'problemsbysv';
    case SLA_REPORT = 'slareport';
    case SVG_GRAPH = 'svggraph';
    case SYSTEM_INFO = 'systeminfo';
    case TOP_HOSTS = 'tophosts';
    case TOP_TRIGGERS = 'toptriggers';
    case TRIGGER_OVERVIEW = 'trigover';
    case URL = 'url';
    case WEB_MONITORING = 'web';

}
