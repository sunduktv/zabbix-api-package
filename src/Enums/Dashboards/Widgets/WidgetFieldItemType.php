<?php

namespace Konstantinkotov\ZabbixApiPackage\Enums\Dashboards\Widgets;

enum WidgetFieldItemType : int
{
    case INTEGER = 0;
    case STRING = 1;
    case HOST_GROUP = 2;
    case HOST = 3;
    case ITEM = 4;
    case ITEM_PROTOTYPE = 5;
    case GRAPH = 6;
    case GRAPH_PROTOTYPE = 7;
    case MAP = 8;
    case SERVICE = 9;
    case SLA = 10;
    case USER = 11;
    case ACTION = 12;
    case MEDIA_TYPE = 13;

}
