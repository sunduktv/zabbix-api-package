<?php

namespace Konstantinkotov\ZabbixApiPackage\Enums\Dashboards\Create;

enum DashboardCreateField : string
{
    case DASHBOARD_ID = 'dashboardid';
    case NAME = 'name';
    case TYPE = 'type';
    case X = 'x';
    case Y = 'y';
    case WIDTH = 'width';
    case HEIGHT = 'height';
    case FIELDS = 'fields';
    case VALUE = 'value';
}
