<?php

namespace Konstantinkotov\ZabbixApiPackage\Builders\Templates;

use Konstantinkotov\ZabbixApiPackage\Builders\DataBuilderCore;

class TemplateParamsBuilder extends DataBuilderCore
{
    public function setOutput(string $field) : void
    {
        $this->data['output'][] = $field;
    }

    public function build() : array
    {
        $data = parent::build();

        $output = $data['output'] ?? "extend";
        $data['output'] = $output;

        return $data;
    }
}