<?php

namespace Konstantinkotov\ZabbixApiPackage\Builders\Dashboards;

use Konstantinkotov\ZabbixApiPackage\Builders\DataBuilderCore;
use Konstantinkotov\ZabbixApiPackage\Enums\Dashboards\DashboardField;

class DashboardParamsBuilder extends DataBuilderCore
{
    public function addOutput(DashboardField $output): void
    {
        $this->data['output'][] = $output->value;
    }

    public function setPages(array $pages) : void
    {
        $this->data['pages'] = $pages;
    }

    public function setDashboardId(string $dashboardId) : void
    {
        $this->data['dashboardid'] = $dashboardId;
    }
}