<?php

namespace Konstantinkotov\ZabbixApiPackage\Builders\Dashboards\Pages;

use Konstantinkotov\ZabbixApiPackage\Builders\Contracts\HasBuildInterface;
use Konstantinkotov\ZabbixApiPackage\Builders\Dashboards\Pages\Widgets\WidgetParamsBuilder;
use Konstantinkotov\ZabbixApiPackage\Builders\DataBuilderCore;

class PageParamsBuilder extends DataBuilderCore
{
    public function setDashboardPageId(string $dashboardPageId): void
    {
        $this->data['dashboard_pageid'] = $dashboardPageId;
    }

    public function addWidget(HasBuildInterface $widget): void
    {
        $this->data['widgets'][] = $widget->build();
    }

    public function setWidgets(array $widgets): void
    {
        $this->data['widgets'] = $widgets;
    }
}