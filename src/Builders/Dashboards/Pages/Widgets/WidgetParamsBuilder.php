<?php

namespace Konstantinkotov\ZabbixApiPackage\Builders\Dashboards\Pages\Widgets;

use Konstantinkotov\ZabbixApiPackage\Builders\Contracts\HasBuildInterface;
use Konstantinkotov\ZabbixApiPackage\Builders\DataBuilderCore;
use Konstantinkotov\ZabbixApiPackage\Enums\Dashboards\Widgets\WidgetType;

class WidgetParamsBuilder extends DataBuilderCore
{
    public function setType(WidgetType $widgetType): void
    {
        $this->data['type'] = $widgetType->value;
    }

    public function setName(string $name): void
    {
        $this->data['name'] = $name;
    }
    public function setPositionX(int $x = 0): void
    {
        $this->data['x'] = $x;
    }
    public function setPositionY(int $y = 0): void
    {
        $this->data['y'] = $y;
    }
    public function setWidth(int $width): void
    {
        $this->data['width'] = $width;
    }
    public function setHeight(int $height): void
    {
        $this->data['height'] = $height;
    }
    public function addField(HasBuildInterface $fieldItemBuilder): void
    {
        $this->data['fields'][] = $fieldItemBuilder->build();
    }
}