<?php

namespace Konstantinkotov\ZabbixApiPackage\Builders\Dashboards\Pages\Widgets;

use Konstantinkotov\ZabbixApiPackage\Builders\DataBuilderCore;
use Konstantinkotov\ZabbixApiPackage\Enums\Dashboards\Widgets\WidgetFieldItemType;

class WidgetFieldItemBuilder extends DataBuilderCore
{
    public function setType(WidgetFieldItemType $widgetFieldItemType): void
    {
        $this->data['type'] = $widgetFieldItemType->value;
    }

    public function setName(string $name): void
    {
        $this->data['name'] = $name;
    }

    public function setValue(string $value): void
    {
        $this->data['value'] = $value;
    }
}