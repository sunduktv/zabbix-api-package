<?php

namespace Konstantinkotov\ZabbixApiPackage\Builders\Triggers;

use Konstantinkotov\ZabbixApiPackage\Builders\DataBuilderCore;

class TriggerParamsBuilder extends DataBuilderCore
{
    public function __construct()
    {
        $this->data['sortfield'] = 'priority';
        $this->data['sortorder'] = 'DESC';
    }

    public function addHostId(int $host_id) : void
    {
        $this->data['hostids'][] = $host_id;
    }

    public function setSortField(string $field)
    {
        $this->data['sortfield'] = $field;
    }

    public function setSortOrder(string $field)
    {
        $this->data['sortorder'] = $field;
    }
}