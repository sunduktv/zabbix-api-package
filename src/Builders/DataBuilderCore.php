<?php

namespace Konstantinkotov\ZabbixApiPackage\Builders;

use Konstantinkotov\ZabbixApiPackage\Builders\Contracts\DataBuilderCoreInterface;

class DataBuilderCore implements DataBuilderCoreInterface
{
    protected array $data = [];

    function build() : array
    {
        return $this->data;
    }

    public function addParams(array $params) : void
    {
        $this->data = [
            ...$this->data,
            ...$params
        ];
    }

    public function count() : int
    {
        return count($this->data);
    }

    public function addFilter(array $filterArray): void
    {
        $this->data['filter'] = [
            ...($this->data['filter'] ?? []),
            ...$filterArray
        ];
    }
}