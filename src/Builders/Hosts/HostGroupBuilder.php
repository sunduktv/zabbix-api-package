<?php

namespace Konstantinkotov\ZabbixApiPackage\Builders\Hosts;

use Konstantinkotov\ZabbixApiPackage\Builders\DataBuilderCore;

class HostGroupBuilder extends DataBuilderCore
{
    public function setGroupId(string $group_id) : void
    {
        $this->data['groupid'] = $group_id;
    }
}