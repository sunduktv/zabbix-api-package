<?php

namespace Konstantinkotov\ZabbixApiPackage\Builders\Hosts;

use Konstantinkotov\ZabbixApiPackage\Builders\DataBuilderCore;

class HostInterfacesBuilder extends DataBuilderCore
{
    public function setType(int $type = 1) : void
    {
        $this->data['type'] = $type;
    }
    public function setMain(int $main = 1) : void
    {
        $this->data['main'] = $main;
    }
    public function setUseIp(int $useIp = 1) : void
    {
        $this->data['useip'] = $useIp;
    }
    public function setIp(string $ip) : void
    {
        $this->data['ip'] = $ip;
    }
    public function setDns(string $dns = "") : void
    {
        $this->data['dns'] = $dns;
    }
    public function setPort(string $port = "10050") : void
    {
        $this->data['port'] = $port;
    }
    public function setInterfaceRef(string $interfaceRef) : void
    {
        $this->data['interface_ref'] = $interfaceRef;
    }
}