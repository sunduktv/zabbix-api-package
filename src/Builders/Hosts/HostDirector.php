<?php

namespace Konstantinkotov\ZabbixApiPackage\Builders\Hosts;

use Konstantinkotov\ZabbixApiPackage\Builders\DataBuilderCore;

class HostDirector extends DataBuilderCore
{
    /**
     * Use when update or create a record
     *
     * @param string $hostname
     * @return void
     */
    public function setHostName(string $hostname) : void
    {
        $this->data['host'] = $hostname;
    }

    /**
     * Use when update or create a record
     *
     * @param DataBuilderCore $hostInterfacesBuilder
     * @return void
     */

    public function setInterfaces(DataBuilderCore $hostInterfacesBuilder) : void
    {
        $this->data['interfaces'][] = $hostInterfacesBuilder->build();
    }

    /**
     * Use when update or create a record
     *
     * @param DataBuilderCore $hostGroupBuilder
     * @return void
     */

    public function setGroups(DataBuilderCore $hostGroupBuilder) : void
    {
        $this->data['groups'][] = $hostGroupBuilder->build();
    }

    /**
     * Use when update or create a record
     *
     * @param DataBuilderCore $hostTemplateBuilder
     * @return void
     */

    public function setTemplates(DataBuilderCore $hostTemplateBuilder) : void
    {
        $data = $hostTemplateBuilder->build();

        if(\is_array($data)){
            foreach ($data as $template) {
                $this->data['templates'][] = $template;
            }
        } else {
            $this->data['templates'][] = $hostTemplateBuilder->build();
        }
    }

    /**
     * Use when update a record
     *
     * @param string $id
     * @return void
     */

    public function setId(string $id): void{
        $this->data['id'] = $id;
    }
}