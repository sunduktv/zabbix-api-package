<?php

namespace Konstantinkotov\ZabbixApiPackage\Builders\Hosts;

use Konstantinkotov\ZabbixApiPackage\Builders\DataBuilderCore;

class HostTemplateBuilder extends DataBuilderCore
{
    public function setTemplateId(string $template_id) : void
    {
        $this->data['templateid'] = $template_id;
    }
}