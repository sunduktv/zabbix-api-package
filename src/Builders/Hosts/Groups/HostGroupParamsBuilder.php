<?php

namespace Konstantinkotov\ZabbixApiPackage\Builders\Hosts\Groups;

use Konstantinkotov\ZabbixApiPackage\Builders\DataBuilderCore;

class HostGroupParamsBuilder extends DataBuilderCore
{
    public function addOutput(string $field): void
    {
        $this->data['output'][] = $field;
    }

    public function build() : array
    {
        $data = parent::build();

        $output = $data['output'] ?? 'extend';
        $data['output'] = $output;

        return $data;
    }
}