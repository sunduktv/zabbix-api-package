<?php

namespace Konstantinkotov\ZabbixApiPackage\Builders\Contracts;

interface HasBuildInterface
{
    function build(): array;
}