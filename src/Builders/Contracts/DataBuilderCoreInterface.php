<?php

namespace Konstantinkotov\ZabbixApiPackage\Builders\Contracts;

interface DataBuilderCoreInterface extends HasBuildInterface
{
    public function addParams(array $params): void;

    public function count() : int;
}